
fetch(`https://ajax.test-danit.com/api/swapi/films`)
  .then(res => res.json())
  .then(data => {
    data.forEach(({ characters, episodeId, name, openingCrawl }) => {

      const actorsPromises = characters.map(actor =>
        fetch(actor)
          .then(res => res.json()));
      Promise.all(actorsPromises)
        .then(actorsList => {
          new filmCard(actorsList, episodeId, name, openingCrawl).render();
        }).catch(err => {
          console.log("Щось із списком акторів на сервері (((");
        })
    });
  }).catch(err => {
    console.log("Щось із сервером (((");
  })


class filmCard {
  constructor(actorsList, episodeId, name, openingCrawl) {
    this.actorsList = actorsList;
    this.episodeId = episodeId;
    this.name = name;
    this.openingCrawl = openingCrawl;
  }

  render() {
    const div = document.querySelector('.container');
    const actorsInsert = this.actorsList.map(actor => `<span>${actor.name}</span>`).join(', ');
    const card = `
      <div class="movieCard">
        <h1 class="name">${this.name}</h1>
        <h3 class="episodeId">episode ID #${this.episodeId}</h3>
        <div class="characters">${actorsInsert}</div>
        <div class="openingCrawl">${this.openingCrawl}</div>
      </div>`;
    div.insertAdjacentHTML("beforeend", card);
  }
}
